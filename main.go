package main

import (
	"crypto/sha256"
	"fmt"
	"os"
	"runtime"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/brurberg/log/v2"
	libDB "gitlab.com/brurbergauth/backend/db"
	"gitlab.com/brurbergauth/backend/login"
	"gitlab.com/brurbergauth/backend/register"
	"gitlab.com/brurbergauth/backend/update"
	"gitlab.com/brurberglogs/backend/utils"
)

func loadEnvVar() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
}

type DB struct {
	DB *libDB.DB
}

func TraceMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		sum := sha256.Sum256([]byte(fmt.Sprint(*c.Request)))
		if c.Keys == nil {
			c.Keys = make(map[string]interface{})
		}
		c.Keys["trace"] = fmt.Sprintf("%x", sum)
		c.Next()
	}
}

func main() {
	loadEnvVar()
	log.LogHandler = log.Logger{
		Level: log.Clean,
		Out: log.ExternLogger{
			Endpoint: "https://log.brurberg.no/api/newentry",
			LogToPackage: utils.PackageConsts{
				Domain: "auth.brurberg.no",
				Key:    "Super Secret Key",
			}.LogToPackage,
		},
	}

	dbCon, dbErr := libDB.DBConnect{DB_HOST: os.Getenv("POSTGRES_HOST"), DB_USER: os.Getenv("POSTGRES_USER"), DB_PASSWORD: os.Getenv("POSTGRES_PASSWORD"), DB_NAME: os.Getenv("POSTGRES_DB")}.ConnectToDB()
	if dbErr != nil {
		log.Fatal(dbErr)
	}
	db := DB{DB: &libDB.DB{Conn: dbCon}}
	login := login.DB{DB: db.DB}
	reg := register.DB{DB: db.DB}
	upd := update.DB{DB: db.DB}

	r := gin.Default()
	trace := r.Group("/")
	trace.Use(TraceMiddleware())

	api := trace.Group("/api")
	api.POST("/registeruser", reg.RegisterUserEndpoint)
	api.POST("/authenticate", login.CreateTokenEndpoint)
	api.POST("/resetpassword", upd.ResetPassEndpoint)

	token := api.Group("/")
	token.Use(db.TokenMiddleware())
	token.POST("/updatepassword", upd.UpdatePassEndpoint)

	auth := token.Group("/")
	auth.Use(db.AuthMiddleware())
	auth.POST("/verify-otp", db.VerifyOtpEndpoint) //This might need a look at a later time.

	mfa := api.Group("/")
	mfa.Use(db.MFAMiddleware())
	mfa.POST("/registersystem", reg.RegisterSystemEndpoint) //Might add a return later, but that will be when it is out of development
	mfa.POST("/registersystemconsent", reg.RegisterSystemConsentEndpoint)
	mfa.POST("/verifyemail", reg.VerifyEmailEndpoint)
	mfa.GET("/specs", func(c *gin.Context) {
		c.JSON(200, gin.H{"OS": runtime.GOOS, "Threads": runtime.NumCPU()})
	})
	// mfa.POST("/updateuserinfo", upd.UpdateUserEndpoint) //TODO: Pushback to after rewrite
	// mfa.POST("/updateclaims", upd.ClaimsEndpoint)

	log.CheckError("Start server", r.Run(":3000"), log.Critical)
}
