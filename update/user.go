package update

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/dgryski/dgoogauth"
	"github.com/gin-gonic/gin"
	"gitlab.com/brurberg/log/v2"
	libDB "gitlab.com/brurbergauth/backend/db"
	"gitlab.com/brurbergauth/backend/lib/jwt"
	req "gitlab.com/brurbergauth/backend/lib/req/gin"
)

var master_host = os.Getenv("MASTER_HOST")

type DB struct {
	DB *libDB.DB
}

func (db DB) UpdateUserEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return
	}
	var user libDB.User
	err = json.Unmarshal(body, &user)
	if log.CheckError("UpdateUser", err, log.Critical) {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err = db.DB.UpdateUser(user)
	if err != nil {
		if strings.HasPrefix(err.Error(), "Validate userinfo:") {
			if log.CheckError("UpdateUser", err, log.Error) {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			}
		} else {
			if log.CheckError("UpdateUser", err, log.Error) {
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			}
		}
	}
}

func (db DB) VerifyJwtAndGetClaims(c *gin.Context) (map[string]interface{}, []byte) {
	bearerToken, err := req.GetBearerToken(c.Request.Header.Get("authorization"))
	if log.CheckError("VerifyJwtAndGetClaims", err, log.Error) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return nil, nil
	}
	host := c.Request.Host
	if master_host != host {
		c.JSON(http.StatusForbidden, gin.H{"error": "Trying to connect to extern service from unauthorized host"})
		return nil, nil
	}
	secret := db.DB.GetJWTSecret(host)
	if len(secret) < 2 {
		c.JSON(http.StatusForbidden, gin.H{"error": "The host you are trying to logon to is not available"})
		return nil, nil
	}
	log.Write(log.Log{
		Level:   log.Debug,
		Context: "VerifyJwtAndGetClaims",
		Message: "Got secret",
	})
	decodedToken, err := jwt.Verify(bearerToken, secret)
	if log.CheckError("VerifyJwtAndGetClaims", err, log.Error) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return nil, nil
	}
	log.Write(log.Log{
		Level:   log.Success,
		Context: "VerifyJwtAndGetClaims",
		Message: "Verified and god claims",
	})
	return decodedToken, secret
}

func (db DB) VerifyOTP(otp, username string) (validToken bool, err error) {
	otpc := &dgoogauth.OTPConfig{
		Secret:      db.DB.GetOTPSecret(username),
		WindowSize:  3,
		HotpCounter: 0,
	}
	validToken, err = otpc.Authenticate(otp)
	if log.CheckError("VerifyOTP", err, log.Error) {
		return
	}
	return
}

func (db DB) GetSecureSecret(c *gin.Context, user map[string]interface{}) []byte {
	host := c.Request.Host
	if _, ok := user["host"]; ok {
		if master_host != host {
			c.JSON(http.StatusForbidden, gin.H{"error": "Trying to connect to extern service from unauthorized host"})
			return nil
		}
		host = user["host"].(string)
	}
	return db.DB.GetJWTSecret(host)
}

type ResetUserPass struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	OTP      string `json:"otp"`
}

func (db DB) ResetPassEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return
	}
	var user ResetUserPass
	err = json.Unmarshal(body, &user)
	if log.CheckError("ResetUserPass", err, log.Error) {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	verifiedOTP, err := db.VerifyOTP(user.OTP, user.Username)
	if log.CheckError("ResetUserPass", err, log.Error) {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if !verifiedOTP {
		if log.CheckError("ResetUserPass", fmt.Errorf("Invalid OTP token"), log.Error) {
			c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
		}
		return
	}

	pass, email, err := db.DB.ResetPass(libDB.User{Username: user.Username, Email: user.Email})
	if err != nil {
		if _, ok := err.(libDB.ErrEmailIsWrong); ok {
			err = SendWrongEmail(email, user.Email, user.Username)
			if log.CheckError("ResetUserPass", err, log.Error) {
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			}
		} else if strings.HasPrefix(err.Error(), "Validate userinfo:") {
			if log.CheckError("ResetUserPass", err, log.Error) {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			}
		} else {
			if log.CheckError("ResetUserPass", err, log.Error) {
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			}
		}
		return
	}

	err = SendReserPassEmail(email, pass)
	if log.CheckError("ResetUserPass", err, log.Error) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return //TODO: VERIFY THAT IT IS SMART TO RETURN HERE!
	}
	claims := make(map[string]interface{})
	claims["username"] = user.Username
	secret := db.GetSecureSecret(c, nil)
	if secret == nil {
		return
	}
	token, err := jwt.Sign(claims, secret)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		log.Write(log.Log{
			Level:   log.Error,
			Context: "ResetUserPass",
			Message: err.Error(),
		})
		return
	}
	log.Write(log.Log{
		Level:   log.Success,
		Context: "ResetUserPass",
		Message: "Signed",
	})
	c.JSON(http.StatusOK, gin.H{"token": token})
}

type UpadteUserPass struct {
	Pass    string `json:"password"`
	NewPass string `json:"newpassword"`
	OTP     string `json:"otp"`
}

func (db DB) UpdatePassEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return
	}
	var user UpadteUserPass
	err = json.Unmarshal(body, &user)
	if log.CheckError("UpadteUserPass", err, log.Critical) {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	claims := c.Keys["claims"].(map[string]interface{})
	if user.Pass == "" {
		if claims["authorized"] != true {
			errMSG := "User neither authorized nor provided a password"
			log.Write(log.Log{
				Level:   log.Error,
				Context: "UpadteUserPass",
				Message: errMSG,
			})
			c.JSON(http.StatusForbidden, gin.H{"error": errMSG})
			return
		}
	} else {
		correct, err := db.DB.CheckPassword([]byte(user.Pass), claims["username"].(string))
		if log.CheckError("UpadteUserPass", err, log.Error) {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		if !correct {
			errMSG := "Reset password with invalid password"
			log.Write(log.Log{
				Level:   log.Error,
				Context: "UpadteUserPass",
				Message: errMSG,
			})
			c.JSON(http.StatusForbidden, gin.H{"error": errMSG})
			return
		}
	}
	verifiedOTP, err := db.VerifyOTP(user.OTP, claims["username"].(string))
	if log.CheckError("UpadteUserPass", err, log.Error) {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if !verifiedOTP {
		err = fmt.Errorf("Invalid OTP token")
		if log.CheckError("UpadteUserPass", err, log.Error) {
			c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
		}
		return
	}
	email, err := db.DB.UpdatePass(claims["username"].(string), user.NewPass)
	if log.CheckError("UpadteUserPass", err, log.Error) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	err = SendUpdatePassEmail(email)
	if log.CheckError("UpadteUserPass", err, log.Error) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()}) //TODO: This line seems useless
	}
	out := make(map[string]interface{})
	claims["authorized"] = true
	secret := db.DB.GetJWTSecret(claims["host"].(string))
	out["token"], err = jwt.Sign(claims, secret)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		log.Write(log.Log{
			Level:   log.Error,
			Context: "UpdatePass",
			Message: err.Error(),
		})
		return
	}
	log.Write(log.Log{
		Level:   log.Success,
		Context: "UpdatePass",
		Message: "Signed",
	})
	c.JSON(http.StatusOK, out)
}

/*func (db DB) ClaimsEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	bearerToken, _ := helper.GetBearerToken(c.Request.Header.Get("authorization"))
	claims, _ := helper.VerifyJwt(bearerToken, db.DB.GetJWTSecret(c.Request.Host))
	userinfo := db.DB.GetUserInfoForSystem(c.Request.Host, claims["username"].(string))

	c.JSON(http.StatusOK, gin.H{"userinfo": userinfo})
}*/
