package update

import (
	"fmt"
	"net/smtp"
	"os"

	"gitlab.com/brurberg/log/v2"
)

func SendReserPassEmail(address, password string) (err error) {
	// Set up authentication information.
	auth := smtp.PlainAuth(os.Getenv("EMAIL_SENDER"), os.Getenv("EMAIL_USERNAME"), os.Getenv("EMAIL_PASSWORD"), os.Getenv("EMAIL_SERVER"))

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	to := []string{address}
	msg := []byte(fmt.Sprintf("From: %s\r\n", os.Getenv("EMAIL_SENDER")) +
		fmt.Sprintf("To: %s\r\n", address) +
		"Subject: BrurbergAuth password reset\r\n" +
		"\r\n" +
		fmt.Sprintf("Your new password is: %s\r\n", password) +
		"We recommend you change this password.\r\n" +
		"\r\n" +
		" - BrurbergAuth")
	err = smtp.SendMail(os.Getenv("EMAIL_SERVER")+":"+os.Getenv("EMAIL_PORT"), auth, os.Getenv("EMAIL_SENDER"), to, msg)
	if err != nil {
		log.Write(log.Log{
			Level:   log.Error,
			Context: "ResetPasswordEmail",
			Message: err.Error(),
		})
		return
	}
	log.Write(log.Log{
		Level:   log.Success,
		Context: "ResetPasswordEmail",
		Message: "Sent reset password email",
	})
	return
}

func SendUpdatePassEmail(address string) (err error) {
	// Set up authentication information.
	auth := smtp.PlainAuth(os.Getenv("EMAIL_SENDER"), os.Getenv("EMAIL_USERNAME"), os.Getenv("EMAIL_PASSWORD"), os.Getenv("EMAIL_SERVER"))

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	to := []string{address}
	msg := []byte(fmt.Sprintf("From: %s\r\n", os.Getenv("EMAIL_SENDER")) +
		fmt.Sprintf("To: %s\r\n", address) +
		"Subject: BrurbergAuth password updated\r\n" +
		"\r\n" +
		"Password was successfully updated.\r\n" +
		"If you did not update your password, please respond to this mail.\r\n" +
		"\r\n" +
		" - BrurbergAuth")
	err = smtp.SendMail(os.Getenv("EMAIL_SERVER")+":"+os.Getenv("EMAIL_PORT"), auth, os.Getenv("EMAIL_SENDER"), to, msg)
	if err != nil {
		log.Write(log.Log{
			Level:   log.Error,
			Context: "UpdatePasswordEmail",
			Message: err.Error(),
		})
		return
	}
	log.Write(log.Log{
		Level:   log.Success,
		Context: "UpdatePasswordEmail",
		Message: "Sent update password email",
	})
	return
}

func SendWrongEmail(address, falseAddress, username string) (err error) {
	// Set up authentication information.
	auth := smtp.PlainAuth(os.Getenv("EMAIL_SENDER"), os.Getenv("EMAIL_USERNAME"), os.Getenv("EMAIL_PASSWORD"), os.Getenv("EMAIL_SERVER"))

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	to := []string{address}
	msg := []byte(fmt.Sprintf("From: %s\r\n", os.Getenv("EMAIL_SENDER")) +
		fmt.Sprintf("To: %s\r\n", address) +
		"Subject: BrurbergAuth unauthorized password reset atempt\r\n" +
		"\r\n" +
		"There has been a atempt at resetting your password with an incorrect email.\r\n" +
		"If this was you, please consider updating your email.\r\n" +
		"No action has been taken at this time. Your old password is still valid and probably not leaked.\r\n" +
		"\r\n" +
		fmt.Sprintf("Affected username: %s\r\n", username) +
		fmt.Sprintf("Email used: %s\r\n", falseAddress) +
		"If this was not you, please reply to this email and we can try to sort something out.\r\n" +
		"\r\n" +
		" - BrurbergAuth")
	err = smtp.SendMail(os.Getenv("EMAIL_SERVER")+":"+os.Getenv("EMAIL_PORT"), auth, os.Getenv("EMAIL_SENDER"), to, msg)
	if err != nil {
		log.Write(log.Log{
			Level:   log.Error,
			Context: "WrongEmail",
			Message: err.Error(),
		})
		return
	}
	log.Write(log.Log{
		Level:   log.Success,
		Context: "WrongEmail",
		Message: "Sent wrong email email",
	})
	return
}
