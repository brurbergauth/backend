DROP DATABASE IF EXISTS brurbergauth;
CREATE DATABASE brurbergauth;
\c brurbergauth;

CREATE TABLE UserData (
  Username TEXT PRIMARY KEY,
  Salt BYTEA NOT NULL,
  PasswordHash BYTEA NOT NULL,
  MFASecret TEXT,
  Firstname TEXT,
  Sirname TEXT,
  Email TEXT,
  EmailVerification varchar(8),
  Birthday TIMESTAMP,
  Address TEXT,
  Postnumber SMALLINT,
  ConsentForm TEXT NOT NULL,
  ConsentedAt TIMESTAMPTZ DEFAULT Now()
);

CREATE TABLE Systems (
  Host TEXT NOT NULL,
  Revision INT NOT NULL,
  JWTSecret TEXT NOT NULL,
  Username BOOL DEFAULT FALSE,
  FirstName BOOL DEFAULT FALSE,
  LastName BOOL DEFAULT FALSE,
  Email BOOL DEFAULT FALSE,
  AgeRange BOOL DEFAULT FALSE,
  Birthdate BOOL DEFAULT FALSE,
  Address BOOL DEFAULT FALSE,
  Region BOOL DEFAULT FALSE,
  Country BOOL DEFAULT FALSE,
  CreatedAt TIMESTAMPTZ DEFAULT Now(),
  PRIMARY KEY (Host, Revision)
);

CREATE TABLE SystemConsent (
  ID BIGSERIAL PRIMARY KEY,
  Host TEXT NOT NULL,
  Uname TEXT REFERENCES UserData(Username),
  Revision INT NOT NULL,
  AutomaticallyRestricted BOOL DEFAULT FALSE,
  Username BOOL DEFAULT FALSE,
  FirstName BOOL DEFAULT FALSE,
  LastName BOOL DEFAULT FALSE,
  Email BOOL DEFAULT FALSE,
  AgeRange BOOL DEFAULT FALSE,
  Birthdate BOOL DEFAULT FALSE,
  Address BOOL DEFAULT FALSE,
  Region BOOL DEFAULT FALSE,
  Country BOOL DEFAULT FALSE,
  CreatedAt TIMESTAMPTZ DEFAULT Now(),
  UNIQUE (Host, Username, Revision)
);
