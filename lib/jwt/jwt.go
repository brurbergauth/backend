package jwt

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/brurberg/log/v2"
)

type Token struct {
	Token string `json:"token"`
}

var WeakSystemSecret = fmt.Errorf("Not strong enught system secret")

func Sign(claims jwt.MapClaims, secret []byte) (string, error) {
	if len(secret) < 32 { //TODO: remove static lenth of secret string.
		return "", WeakSystemSecret
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(secret)
}

func Verify(token string, secret []byte) (map[string]interface{}, error) {
	jwtToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error signing the token")
		}
		return secret, nil
	})
	if err != nil {
		return nil, err
	}
	if !jwtToken.Valid {
		return nil, fmt.Errorf("Invalid authorization token")
	}
	return jwtToken.Claims.(jwt.MapClaims), nil
}

func GetClaims(token string) (claims map[string]interface{}) {
	parts := strings.Split(token, ".")
	if len(parts) != 3 {
		return
	}
	dec, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		log.Write(log.Log{
			Level:   log.Error,
			Context: "GetClaims",
			Message: err.Error(),
		})
		return
	}
	log.CheckError("GetClaims", json.Unmarshal(dec, &claims), log.Error)
	return
}
