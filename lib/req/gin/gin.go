package gin

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

func GetBearerToken(header string) (string, error) {
	if header == "" {
		return "", fmt.Errorf("An authorization header is required")
	}
	token := strings.Split(header, " ")
	if len(token) != 2 {
		return "", fmt.Errorf("Malformed bearer token")
	}
	return token[1], nil
}

func GetBearerTokenC(c *gin.Context, header string) (string, error) {
	return GetBearerToken(c.Request.Header.Get(header))
}
