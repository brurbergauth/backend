package otp

import (
	"gitlab.com/brurbergauth/backend/lib/crypto"
)

type OtpToken struct {
	Token string `json:"otp"`
}

func GenerateSecret() string {
	return crypto.GenRandBase32String(16)
}
