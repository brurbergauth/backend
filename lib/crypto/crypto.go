package crypto

import (
	"crypto/rand"
	"encoding/base32"
)

func GenRandBase32String(length int) string {
	random := make([]byte, length)
	rand.Read(random)
	return base32.StdEncoding.EncodeToString(random)[:length]
}
