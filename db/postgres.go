package db

import (
	"crypto/rand"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/brurberg/log/v2"
	"gitlab.com/brurbergauth/backend/lib/crypto"
	"gitlab.com/brurbergauth/backend/lib/otp"
	"golang.org/x/crypto/scrypt"
)

type DBConnect struct {
	DB_HOST     string
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string
}

type DB struct {
	Conn *sql.DB
}

type JSONTime time.Time

type User struct {
	Username   string `json:"username"`
	Password   string `json:"password"`
	Firstname  string `json:"firstname"`
	Sirname    string `json:"sirname"`
	Email      string `json:"email"`
	Birthday   string `json:"birthday"`
	Address    string `json:"address"`
	Postnumber int    `json:"postnumber"`
}

func (dbc DBConnect) ConnectToDB() (*sql.DB, error) {
	dbinfo := ""
	if dbc.DB_PASSWORD == "" {
		dbinfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_NAME)
	} else {
		dbinfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			dbc.DB_HOST, dbc.DB_USER, dbc.DB_PASSWORD, dbc.DB_NAME)
	}

	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return nil, err
	}

	return db, db.Ping()
}

func CheckRowAff(context string, result sql.Result, level log.Level) bool {
	rowAff, err := result.RowsAffected()
	if rowAff < 1 {
		if err == nil {
			errMSG := "No rows affected"
			log.Write(log.Log{
				Level:   level,
				Context: context,
				Message: errMSG,
			})
			return true
		}
		log.Write(log.Log{
			Level:   level,
			Context: context,
			Message: err.Error(),
		})
		return true
	}
	return false
}

type Rights struct {
	FirstName bool `json:"firstName"`
	Username  bool `json:"username"`
	LastName  bool `json:"lastName"`
	Email     bool `json:"email"`
	AgeRange  bool `json:"ageRange"`
	Birthdate bool `json:"birthdate"`
	Address   bool `json:"address"`
	Region    bool `json:"region"`
	Country   bool `json:"country"`
}

type System struct {
	Host   string `json:"host"`
	Rights Rights `json:"rights"`
}

func (db DB) RegisterSystem(system System) (err error) {
	result, err := db.Conn.Exec("INSERT INTO systems (host, revision, JWTSecret, Username, FirstName, LastName, Email, AgeRange, Birthdate, Address, Region, Country) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);", system.Host, 0, crypto.GenRandBase32String(64), system.Rights.Username, system.Rights.FirstName, system.Rights.LastName, system.Rights.Email, system.Rights.AgeRange, system.Rights.Birthdate, system.Rights.Address, system.Rights.Region, system.Rights.Country)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}

func (db DB) RegisterSystemConsent(rights Rights, user, host string) (err error) {
	result, err := db.Conn.Exec("INSERT INTO systemconsent (host, revision, uname, Username, FirstName, LastName, Email, AgeRange, Birthdate, Address, Region, Country) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);", host, 0, user, rights.Username, rights.FirstName, rights.LastName, rights.Email, rights.AgeRange, rights.Birthdate, rights.Address, rights.Region, rights.Country)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error")
		}
		return
	}
	return
}

var dirtyHack = []string{"Username", "FirstName", "LastName", "Email", "Birthdate", "Address", "Region", "Country", "AgeRange"}

func (db DB) GetUserInfoForSystem(host, user string) (props []string, userinfo map[string]string, err error) { //TODO: Should move to map instead, wold simplify code alot
	var userConsents [9]bool
	needsNewConsent := false
	if err = db.Conn.QueryRow("SELECT Username, FirstName, LastName, Email, Birthdate, Address, Region, Country, AgeRange FROM systemconsent WHERE host = $1 AND uname = $2 AND revision = (SELECT MAX (revision) FROM systemconsent WHERE host = $1 AND uname = $2);", host, user).Scan(&userConsents[0], &userConsents[1], &userConsents[2], &userConsents[3], &userConsents[4], &userConsents[5], &userConsents[6], &userConsents[7], &userConsents[8]); err != nil {
		if err != sql.ErrNoRows {
			return
		}
		needsNewConsent = true
	}
	log.Write(log.Log{
		Level:   log.Debug,
		Context: "UserConsents",
		Message: fmt.Sprint(userConsents),
	})
	var systemRequires [9]bool
	if err = db.Conn.QueryRow("SELECT Username, FirstName, LastName, Email, Birthdate, Address, Region, Country, AgeRange FROM systems WHERE host = $1 AND revision = (SELECT MAX (revision) FROM systems WHERE host = $1);", host).Scan(&systemRequires[0], &systemRequires[1], &systemRequires[2], &systemRequires[3], &systemRequires[4], &systemRequires[5], &systemRequires[6], &systemRequires[7], &systemRequires[8]); err != nil {
		return
	}
	log.Write(log.Log{
		Level:   log.Debug,
		Context: "SystemRequires",
		Message: fmt.Sprint(systemRequires),
	})
	for i := 0; i < len(systemRequires); i++ {
		if systemRequires[i] != userConsents[i] {
			if !systemRequires[i] {
				userConsents[i] = false
				continue
			}
			needsNewConsent = true
			break
		}
	}
	if needsNewConsent {
		for key, val := range systemRequires {
			if val {
				props = append(props, dirtyHack[key])
			}
		}
	}
	var tmpUserInfo [6]string
	stmt := "SELECT Username, FirstName, SirName, Email, Birthday, Address FROM userdata WHERE username = $1;" //TODO: lol fix sirname and birthday mistake
	if err = db.Conn.QueryRow(stmt, user).Scan(&tmpUserInfo[0], &tmpUserInfo[1], &tmpUserInfo[2], &tmpUserInfo[3], &tmpUserInfo[4], &tmpUserInfo[5]); err != nil {
		return
	}

	userinfo = make(map[string]string)
	for i, val := range userConsents {
		if i >= len(tmpUserInfo) {
			break
		}
		if val {
			userinfo[dirtyHack[i]] = tmpUserInfo[i]
		}
	}

	return
}

func (db DB) GetJWTSecret(host string) (secret []byte) {
	db.Conn.QueryRow("SELECT JWTSecret FROM systems WHERE host = $1;", host).Scan(&secret)
	//log.Println(host, string(secret))
	return
}

func (db DB) GetOTPSecret(username string) (secret string) {
	db.Conn.QueryRow("SELECT MFASecret FROM userdata WHERE username = $1;", strings.ToLower(username)).Scan(&secret)
	return
}

const (
	PW_SALT_BYTES = 32
	PW_HASH_BYTES = 64
)

func (db DB) getSalt(username string) (salt []byte) {
	db.Conn.QueryRow("SELECT salt FROM userdata WHERE username = $1;", username).Scan(&salt)
	return
}

func (db DB) getPasswordHash(username string) (hash []byte) {
	db.Conn.QueryRow("SELECT passwordHash FROM userdata WHERE username = $1;", username).Scan(&hash)
	return
}

func HashPassword(password, salt []byte) ([]byte, error) {
	if len(salt) < PW_SALT_BYTES {
		return nil, errors.New("Too week salt")
	}
	hash, err := scrypt.Key(password, salt, 1<<14, 8, 1, PW_HASH_BYTES)
	return hash, err
}

func validateUserInfo(user User) (birthday time.Time, err error) {
	if len(user.Birthday) > 0 {
		birthday, err = time.Parse("2006-01-02", user.Birthday)
		if err != nil {
			return
		}
		now := time.Now()
		threshold := time.Date(now.Year()-6, now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
		if birthday.After(threshold) {
			err = fmt.Errorf("You need to be atleast 6 years to register")
			return
		}
	}
	if len(user.Password) < 8 {
		err = fmt.Errorf("Password needs to be atleast 8 charecters.")
		return
	}
	var number = regexp.MustCompile(`[0-9]`)
	var lower = regexp.MustCompile(`[a-z]`)
	var upper = regexp.MustCompile(`[A-Z]`)
	//var notCharOrNumber = regexp.MustCompile(`[^a-zA-Z0-9 ,._-]`)

	if false { // Change this if you want to force passwords that contains uppercase lowercase and numbers
		if !(number.MatchString(user.Password) && lower.MatchString(user.Password) && upper.MatchString(user.Password)) {
			err = fmt.Errorf("Password does not meet requrement of number, lower and upercase characters.")
			return
		}
	}
	if len(user.Firstname) <= 2 || number.MatchString(user.Firstname) {
		err = fmt.Errorf("Firstname needs to be atleast 2 chars long and not contain numbers %s", user.Firstname)
		return
	}
	if len(user.Sirname) <= 2 || number.MatchString(user.Sirname) {
		err = fmt.Errorf("Sirname needs to be atleast 2 chars long and not contain numbers %s", user.Sirname)
		return
	}
	var email = regexp.MustCompile(`^[a-zA-Z0-9.!#$%&'*+/=?^_{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$`)
	if len(user.Email) > 0 && !email.MatchString(user.Email) {
		err = fmt.Errorf("Your email does not match email format")
		return
	}
	/*if len(user.Address) == 0 && notCharOrNumber.MatchString(user.Address) {
		err = fmt.Errorf("Address can only contain charecters, numbers and spaces")
		return
	}*/
	/*if len(user.Postnumber) > 0 && notCharOrNumber.MatchString(user.Postnumber) {
		err = fmt.Errorf("Postnumber can only contain charecters, numbers, spaces and ,._-")
		return
	}*/
	return
}

func (db DB) RegisterUser(user User, consent string, mailInstedOfMFA bool) (otpSecret, emailVerificationCode string, err error) {
	defer func() {
		if err != nil {
			log.Write(log.Log{
				Level:   log.Error,
				Context: "RegisterUser",
				Message: err.Error(),
			})
		}
	}()
	if consent == "" {
		err = fmt.Errorf("Consent for GDPR form is needed to store userdata and therfore creating a user.")
		log.Write(log.Log{
			Level:   log.Error,
			Context: "RegisterUser",
			Message: err.Error(),
		})
		return
	}

	/*
		if mailInstedOfMFA && len(user.Email) == 0 {
			err = fmt.Errorf("Email was not provided when verifying with mail insted of MFA")
			log.Write(log.Log{
				Level:   log.Error,
				Context: "RegisterUser",
				Message: err.Error(),
			})
			return
		}
	*/
	birthday, err := validateUserInfo(user)
	if err != nil {
		err = fmt.Errorf("Validate userinfo: %v", err)
		log.Write(log.Log{
			Level:   log.Error,
			Context: "RegisterUser",
			Message: err.Error(),
		})
		return
	}

	salt := make([]byte, PW_SALT_BYTES)
	_, err = io.ReadFull(rand.Reader, salt)
	if err != nil {
		//log.Fatal(err)
		log.Write(log.Log{
			Level:   log.Critical,
			Context: "RegisterUser",
			Message: err.Error(),
		})
		os.Exit(1) //TODO: Change this to a return?
	}
	hash, err := HashPassword([]byte(user.Password), salt)
	if log.CheckError("RegisterUser", err, log.Error) {
		return
	}
	if !mailInstedOfMFA {
		otpSecret = otp.GenerateSecret()
	}
	if len(user.Email) != 0 {
		emailVerificationCode = crypto.GenRandBase32String(8)
	}

	result, err := db.Conn.Exec("INSERT INTO userdata (username, salt, passwordHash, MFASecret, Firstname, Sirname, Email, EmailVerification, Birthday, Address, Postnumber, ConsentForm, ConsentedAt) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13);", strings.ToLower(user.Username), string(salt), string(hash), otpSecret, user.Firstname, user.Sirname, user.Email, emailVerificationCode, birthday, user.Address, uint8(user.Postnumber), consent, time.Now())
	if log.CheckError("RegisterUser", err, log.Error) {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error, less then one row affected")
			log.Write(log.Log{
				Level:   log.Error,
				Context: "RegisterUser",
				Message: err.Error(),
			})
		}
		return
	}
	return
}

func (db DB) VerifyEmail(username string) (emailVCode string) {
	db.Conn.QueryRow("SELECT EmailVerification FROM userdata WHERE username = $1;", strings.ToLower(username)).Scan(&emailVCode)
	return
}

func (db DB) UpdateUser(user User) (err error) {
	birthday, err := validateUserInfo(user)
	if err != nil {
		err = fmt.Errorf("Validate userinfo: %v", err)
		return
	}

	query := "UPDATE userdata SET Firstname = $1, Sirname = $2, Email = $3, Birthday = $4, Address = $5, Postnumber = $6 WHERE username = $7"
	result, err := db.Conn.Exec(query, user.Firstname, user.Sirname, user.Email, birthday, user.Address, user.Postnumber, strings.ToLower(user.Username))
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error, less then one row affected")
		}
		return
	}
	return
}

type ErrEmailIsWrong struct {
	text string
}

func (err ErrEmailIsWrong) Error() string {
	return err.text
}

func (db DB) ResetPass(user User) (pass, email string, err error) {
	log.Write(log.Log{
		Level:   log.Debug,
		Context: "ResetPass",
		Message: user.Username,
	})
	db.Conn.QueryRow("SELECT Email FROM userdata WHERE username = $1;", strings.ToLower(user.Username)).Scan(&email)
	if email == "" {
		err = fmt.Errorf("Username is not valid")
		return
	}
	if user.Email != email {
		err = ErrEmailIsWrong{text: "Email provided is not the same as the one stored"}
		return
	}

	pass = crypto.GenRandBase32String(16)
	salt := make([]byte, PW_SALT_BYTES)
	_, err = io.ReadFull(rand.Reader, salt)
	if log.CheckError("ResetPass", err, log.Error) {
		return
	}
	hash, err := HashPassword([]byte(pass), salt)
	if log.CheckError("ResetPass", err, log.Error) {
		return
	}

	query := "UPDATE userdata SET salt = $2, passwordHash = $3 WHERE username = $1"
	result, err := db.Conn.Exec(query, strings.ToLower(user.Username), salt, hash)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error, less then one row affected")
		}
		return
	}
	return
}

func (db DB) UpdatePass(username, newpass string) (email string, err error) {
	username = strings.ToLower(username)
	db.Conn.QueryRow("SELECT Email FROM userdata WHERE username = $1;", username).Scan(&email)
	if email == "" {
		err = fmt.Errorf("Username is not valid")
		return
	}

	salt := make([]byte, PW_SALT_BYTES)
	_, err = io.ReadFull(rand.Reader, salt)
	if log.CheckError("ResetPass", err, log.Error) {
		return
	}
	hash, err := HashPassword([]byte(newpass), salt)
	if log.CheckError("ResetPass", err, log.Error) {
		return
	}

	query := "UPDATE userdata SET salt = $2, passwordHash = $3 WHERE username = $1"
	result, err := db.Conn.Exec(query, username, salt, hash)
	if err != nil {
		return
	}
	rowsAff, err := result.RowsAffected()
	if rowsAff < 1 || err != nil {
		if err == nil {
			err = fmt.Errorf("Database error, less then one row affected")
		}
		return
	}
	return
}

func checkHashes(a, b []byte) bool {
	if (a == nil) || (b == nil) {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}

func (db DB) CheckPassword(password []byte, username string) (bool, error) {
	username = strings.ToLower(username)
	hash, err := HashPassword(password, db.getSalt(username))
	if err != nil {
		return false, err
	}

	return checkHashes(hash, db.getPasswordHash(username)), nil
}
