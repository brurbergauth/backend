package login

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/brurberg/log/v2"
	libDB "gitlab.com/brurbergauth/backend/db"
	"gitlab.com/brurbergauth/backend/lib/jwt"
)

type DB struct {
	DB *libDB.DB
}

var master_host = os.Getenv("MASTER_HOST")

func (db DB) CreateTokenEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	log.Write(log.Log{
		Level:   log.Info,
		Context: "Login",
		Message: "Started",
	})
	body, err := ioutil.ReadAll(c.Request.Body)
	if log.CheckError("Login", err, log.Error) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	user := make(map[string]interface{})
	err = json.Unmarshal(body, &user)
	if log.CheckError("Login", err, log.Error) {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	host := c.Request.Host
	if h, ok := user["host"]; ok {
		if master_host != host {
			c.JSON(http.StatusForbidden, gin.H{"error": "Trying to connect to extern service from unauthorized host"})
			return
		}
		host = h.(string)
	}
	secret := db.DB.GetJWTSecret(host)
	if len(secret) < 2 {
		c.JSON(http.StatusForbidden, gin.H{"error": "The host you are trying to logon to is not available"})
		return
	}
	ok, err := db.DB.CheckPassword([]byte(user["password"].(string)), user["username"].(string))
	delete(user, "password")
	if err != nil {
		log.CheckError("Login", err, log.Error)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	} else if !ok {
		err = fmt.Errorf("Wrong password")
		log.CheckError("Login", err, log.Error)
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
		return
	}
	user["authorized"] = false
	tokenString, err := jwt.Sign(user, secret)
	if err != nil {
		if err == jwt.WeakSystemSecret {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		} else {
			c.JSON(http.StatusForbidden, gin.H{"error": "Wrong username or passowrd"})
		}
		log.Write(log.Log{
			Level:   log.Error,
			Context: "Login",
			Message: "Failed",
		})
		return
	}
	log.Write(log.Log{
		Level:   log.Success,
		Context: "Login",
		Message: "Complete",
	})
	json.NewEncoder(c.Writer).Encode(jwt.Token{Token: tokenString})
}
