package main

import (
	"encoding/json"
	"net/http"
	"os"
	"strings"

	"github.com/dgryski/dgoogauth"
	"github.com/gin-gonic/gin"
	"gitlab.com/brurberg/log/v2"
	"gitlab.com/brurbergauth/backend/lib/jwt"
	req "gitlab.com/brurbergauth/backend/lib/req/gin"
)

type Otp struct {
	Token string `json:"otp"`
	Host  string `json:"host"`
}

var master_host = os.Getenv("MASTER_HOST")

func (db *DB) TokenMiddleware() gin.HandlerFunc {
	context := "Username & Password token check"
	return func(c *gin.Context) {
		log.Write(log.Log{
			Level:   log.Info,
			Context: context,
			Message: "Started",
		})
		bearerToken, err := req.GetBearerToken(c.Request.Header.Get("authorization"))
		if log.CheckError(context, err, log.Warning) {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			c.Abort()
			return
		}
		claims := jwt.GetClaims(bearerToken)
		if c.Keys == nil {
			c.Keys = make(map[string]interface{})
		}
		c.Keys["claims"] = claims
		var host string
		if h, ok := claims["host"]; ok && c.Request.Host == master_host {
			host = h.(string)
		} else {
			host = c.Request.Host
			claims["host"] = host
		}
		log.Write(log.Log{
			Level:   log.Warning,
			Context: context,
			Message: "Host is found",
		})
		_, err = jwt.Verify(bearerToken, db.DB.GetJWTSecret(host))
		if log.CheckError(context, err, log.Warning) {
			c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
			c.Abort()
			return
		}
		log.Write(log.Log{
			Level:   log.Success,
			Context: context,
			Message: "Authorized",
		})
		c.Next()
	}
}

func (db *DB) AuthMiddleware() gin.HandlerFunc {
	context := "Username & Password auth check"
	return func(c *gin.Context) {
		log.Write(log.Log{
			Level:   log.Info,
			Context: context,
			Message: "Started",
		})
		claims := c.Keys["claims"].(map[string]interface{})
		if _, ok := claims["authorized"]; !ok {
			log.Write(log.Log{
				Level:   log.Warning,
				Context: context,
				Message: "Unauthorized",
			})
			c.Abort()
			return
		}
		log.Write(log.Log{
			Level:   log.Success,
			Context: context,
			Message: "Authorized",
		})
		c.Next()
	}
}

func (db DB) MFAMiddleware() gin.HandlerFunc {
	context := "MFAMiddleware"
	return func(c *gin.Context) {
		log.Write(log.Log{
			Level:   log.Info,
			Context: context,
			Message: "Started",
		})
		claims := c.Keys["claims"].(map[string]interface{})
		if authorized, ok := claims["authorized"]; !ok && authorized != true {
			msg := "2FA is required"
			log.Write(log.Log{
				Level:   log.Error,
				Context: context,
				Message: msg,
			})
			c.JSON(http.StatusForbidden, gin.H{"error": msg})
			c.Abort()
			return
		}
		c.Next()
		log.Write(log.Log{
			Level:   log.Success,
			Context: context,
			Message: "Verified",
		})
	}
}

func (db DB) VerifyOtpEndpoint(c *gin.Context) {
	context := "OTP check"
	log.Write(log.Log{
		Level:   log.Info,
		Context: context,
		Message: "Started",
	})
	var otp Otp
	err := json.NewDecoder(c.Request.Body).Decode(&otp)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Write(log.Log{
		Level:   log.Debug,
		Context: context,
		Message: "Decoded bearer token",
	})
	claims := c.Keys["claims"].(map[string]interface{})
	secret := db.DB.GetJWTSecret(claims["host"].(string))
	if len(secret) < 2 {
		c.JSON(http.StatusForbidden, gin.H{"error": "The host you are trying to logon to is not available"})
		return
	}
	log.Write(log.Log{
		Level:   log.Debug,
		Context: context,
		Message: "Got secret",
	})
	otpc := &dgoogauth.OTPConfig{
		Secret:      db.DB.GetOTPSecret(claims["username"].(string)),
		WindowSize:  3,
		HotpCounter: 0,
	}
	claims["authorized"], err = otpc.Authenticate(otp.Token)
	log.CheckError(context, err, log.Error)

	out := make(map[string]interface{})
	if claims["authorized"] == false {
		out["error"] = "Invalid one-time password"
		c.JSON(http.StatusForbidden, out)
		log.Write(log.Log{
			Level:   log.Error,
			Context: context,
			Message: out["error"].(string),
		})
		return
	}
	log.Write(log.Log{
		Level:   log.Debug,
		Context: context,
		Message: "Authorized",
	})

	if claims["host"].(string) != c.Request.Host { //TODO: Temp fix, should get all saved info instead
		props, userInfo, err := db.DB.GetUserInfoForSystem(claims["host"].(string), claims["username"].(string))
		if log.CheckError(context, err, log.Error) {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "There was a error trying to verify your consent"})
			return
		}
		for key, val := range userInfo {
			claims[strings.ToLower(key)] = val
		}
		if len(props) > 0 {
			out["props"] = props
		}
		if _, ok := userInfo["Username"]; !ok {
			delete(claims, "username")
		}
	}
	log.Write(log.Log{
		Level:   log.Debug,
		Context: context,
		Message: "Props",
	})

	out["token"], err = jwt.Sign(claims, secret)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		log.Write(log.Log{
			Level:   log.Error,
			Context: context,
			Message: err.Error(),
		})
		return
	}
	log.Write(log.Log{
		Level:   log.Success,
		Context: context,
		Message: "Signed",
	})
	c.JSON(http.StatusOK, out)
	//json.NewEncoder(c.Writer).Encode(jwToken)
}
