package register

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/brurberg/log/v2"
	libDB "gitlab.com/brurbergauth/backend/db"
	"gitlab.com/brurbergauth/backend/lib/jwt"
)

type DB struct {
	DB *libDB.DB
}

var master_host = os.Getenv("MASTER_HOST")

type registerUser struct {
	User            libDB.User `json:"user"`
	Consent         string     `json:"gdprConsentID"`
	MailInstedOfMFA bool       `json:"mailInstedOfMFA"`
}

func (db DB) RegisterUserEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		return
	}
	var user registerUser
	err = json.Unmarshal(body, &user)
	if log.CheckError("RegisterUser", err, log.Error) {
		return
	}

	otpSecret, emailVerificationCode, err := db.DB.RegisterUser(user.User, user.Consent, user.MailInstedOfMFA) //TODO: since MailInstedOfMFA is kinda deprecated, should it be removed? Could break users if someone scripts registration.
	if log.CheckError("RegisterUser", err, log.Error) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	if err = SendValidationEmail(user.User.Email, emailVerificationCode, user.User.Username); err != nil {
		log.Write(log.Log{
			Level:   log.Error,
			Context: "RegisterUser",
			Message: err.Error(),
		})
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	userM := make(map[string]interface{})
	userM["username"] = user.User.Username
	userM["password"] = user.User.Password
	userM["authorized"] = false

	tokenString, err := jwt.Sign(userM, db.DB.GetJWTSecret(c.Request.Host))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"otp_secret": otpSecret, "token": tokenString})
}

type verifyMail struct {
	EmailVCode string `json:"code"`
}

func (db DB) VerifyEmailEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if log.CheckError("VerifyEmail", err, log.Critical) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	var verify verifyMail
	err = json.Unmarshal(body, &verify)
	if log.CheckError("VerifyEmail", err, log.Error) {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	claims := c.Keys["claims"].(map[string]interface{})
	emailVCode := db.DB.VerifyEmail(claims["username"].(string))
	if emailVCode != verify.EmailVCode {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Verification code was not correct", "verified": false})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Success, email verified", "verified": true})
}

func (db DB) RegisterSystemEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if log.CheckError("RegisterSystem", err, log.Critical) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	var sys libDB.System
	err = json.Unmarshal(body, &sys)
	if log.CheckError("RegisterSystem", err, log.Critical) {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err = db.DB.RegisterSystem(sys)
	if log.CheckError("RegisterSystem", err, log.Error) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	//json.NewEncoder(c.Writer).Encode(otpSecret)
}

func (db DB) RegisterSystemConsentEndpoint(c *gin.Context) {
	defer c.Request.Body.Close()
	body, err := ioutil.ReadAll(c.Request.Body)
	if log.CheckError("RegisterSystemConsent", err, log.Critical) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	var consent libDB.System
	err = json.Unmarshal(body, &consent)
	if log.CheckError("RegisterSystemConsent", err, log.Critical) {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if c.Request.Host != master_host {
		errMSG := "Trying to register consent from unauthorized host"
		log.Write(log.Log{Level: log.Error, Context: "RegisterSystemConsent", Message: errMSG})
		c.JSON(http.StatusForbidden, gin.H{"error": errMSG})
		return
	}
	if consent.Host == master_host {
		errMSG := "Auth host concent is registered another way"
		log.Write(log.Log{Level: log.Error, Context: "RegisterSystemConsent", Message: errMSG})
		c.JSON(http.StatusForbidden, gin.H{"error": errMSG})
		return
	}
	secret := db.DB.GetJWTSecret(consent.Host)
	claims := c.Keys["claims"].(map[string]interface{})

	err = db.DB.RegisterSystemConsent(consent.Rights, claims["username"].(string), consent.Host)
	if log.CheckError("RegisterSystemConsent", err, log.Error) {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	if !consent.Rights.Username {
		delete(claims, "username")
	}
	out, err := jwt.Sign(claims, secret)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	log.Write(log.Log{
		Level:   log.Success,
		Context: "RegisterSystemConsent",
		Message: "Signed",
	})
	c.JSON(http.StatusOK, gin.H{"token": out})
}
