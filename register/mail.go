package register

import (
	"fmt"
	"net/smtp"
	"os"

	"gitlab.com/brurberg/log/v2"
)

func SendValidationEmail(address, verificationCode, username string) (err error) {
	// Set up authentication information.
	auth := smtp.PlainAuth(os.Getenv("EMAIL_SENDER"), os.Getenv("EMAIL_USERNAME"), os.Getenv("EMAIL_PASSWORD"), os.Getenv("EMAIL_SERVER"))

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	to := []string{address}
	msg := []byte(fmt.Sprintf("From: %s\r\n", os.Getenv("EMAIL_SENDER")) +
		fmt.Sprintf("To: %s\r\n", address) +
		"Subject: BrurbergAuth verification Email\r\n" +
		"\r\n" +
		fmt.Sprintf("Username: %s\r\n", username) +
		fmt.Sprintf("Verification code: %s\r\n", verificationCode) +
		fmt.Sprintf("https://auth.brurberg.no/verifyemail/%s\r\n", verificationCode) +
		"\r\n" +
		" - BrurbergAuth")
	err = smtp.SendMail(os.Getenv("EMAIL_SERVER")+":"+os.Getenv("EMAIL_PORT"), auth, os.Getenv("EMAIL_SENDER"), to, msg)
	if err != nil {
		log.Write(log.Log{
			Level:   log.Error,
			Context: "ValidationEmail",
			Message: err.Error(),
		})
		return
	}
	log.Write(log.Log{
		Level:   log.Success,
		Context: "ValidationEmail",
		Message: "Sent wrong email email",
	})
	return
}
